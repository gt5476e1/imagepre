class Book < ActiveRecord::Base
    
    has_attached_file :book_img, :path => ":rails_root/public/system/users/images/:id/:style/:filename",
     :url => "/system/users/images/:id/:style/:filename", styles: { book_index: "250x350>", book_show: "325x475>" }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :book_img, content_type: /\Aimage\/.*\z/
    
    # validates_attachment_content_type :book_img, :content_type => [/\Aimage\/.*\Z/, /\Avideo\/.*\Z/, /\Aaudio\/.*\Z/, /\Aapplication\/.*\Z/]
  
end
