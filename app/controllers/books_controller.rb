class BooksController < ApplicationController
    def index
        @books = Book.all.order("created_at DESC")
    end
    
    def new
        @book = Book.new
    end
    
    def show
        @book = Book.find(params[:id])
    end
    
    def create
        @book = Book.new(book_params)
        
        if @book.save
            redirect_to Book.last
        else
            render 'new'
        end
    end
    
    private
        def book_params
           params.require(:book).permit(:title, :description, :author, :category_id, :book_img) 
        end

end
